﻿using UnityEngine;
using System.Collections.Generic;

namespace TG {
	public static class Core
	{
		static List<object> _subsystems = new List<object>();

		public static List<object> subsystems {
			get {
				return _subsystems;
			}
		}

		public static void Register<T>(T subsystem)
		{
			var s = GetInternal<T>();
			if (s == null) {
				_subsystems.Add(subsystem);
				Debug.Log("subsystem: " + typeof(T));
			} else {
				Debug.LogError("Subsystem already registered: " + typeof(T));
			}
		}

		public static void Unregister<T>(T subsystem) where T : class
		{
			var s = GetInternal<T>();
			if (s != null) {
				if (s == subsystem) {
					_subsystems.Remove(subsystem);
					Debug.Log("subsystem unregister: " + typeof(T));
				} else {
					Debug.LogError("Type and reference mismatch");
				}
			} else {
				Debug.LogError("Subsystem not registered: " + typeof(T));
			}
		}

		public static bool Exists<T>()
		{
			var rv = GetInternal<T>();
			if (rv == null) return false;
				return true;
		}

		public static void Unregister<T>()
		{
			var s = GetInternal<T>();
			if (s != null) {
				_subsystems.Remove(s);
			}
		}

		public static object Get(System.Type type)
		{
			var rv = GetInternal(type);
			if (rv == null) {
				Debug.LogError("Subsystem not found " + type);
			}
			return rv;
		}

		public static T Get<T>()
		{
			var rv = GetInternal<T>();
			if (rv == null) {
				Debug.LogError("Subsystem not found " + typeof(T));
			}
			return rv;
		}

		static T GetInternal<T>()
		{
			return (T)_subsystems.Find(m => m is T);
		}

		static object GetInternal(System.Type type)
		{
			return _subsystems.Find(m => m.GetType() == type);
		}
	}
}