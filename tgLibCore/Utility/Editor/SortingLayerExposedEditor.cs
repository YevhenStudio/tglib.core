﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Reflection;

namespace TG {
	[CustomEditor(typeof(SortingLayerExposed))]
	public class SortingLayerExposedEditor: Editor
	{
		public override void OnInspectorGUI()
		{
			// Get the renderer from the target object
			var renderer = (target as SortingLayerExposed).gameObject.GetComponent<Renderer>();

			// If there is no renderer, we can't do anything
			if (!renderer) { return; }

			// Expose the sorting layer name
			var layerNames = GetSortingLayerNames();

			var oldSortingLayerName = renderer.sortingLayerName;
			if (oldSortingLayerName == "") oldSortingLayerName = "Default";

			var index = System.Array.IndexOf(layerNames, oldSortingLayerName);
			index = EditorGUILayout.Popup("Sorting Layer", index, layerNames, EditorStyles.popup);
			string newSortingLayerName = (index == -1) ? "" : layerNames[index];

			if (newSortingLayerName == "Default") newSortingLayerName = "";

			// string newSortingLayerName = EditorGUILayout.TextField("Sorting Layer Name", renderer.sortingLayerName);
			if (newSortingLayerName != renderer.sortingLayerName) {
				Undo.RecordObject(renderer, "Edit Sorting Layer Name");
				renderer.sortingLayerName = newSortingLayerName;
				EditorUtility.SetDirty(renderer);
			}

			/*
			// Expose the sorting layer ID
			int newSortingLayerId = EditorGUILayout.IntField("Sorting Layer ID", renderer.sortingLayerID);
			if (newSortingLayerId != renderer.sortingLayerID) {
				Undo.RecordObject(renderer, "Edit Sorting Layer ID");
				renderer.sortingLayerID = newSortingLayerId;
				EditorUtility.SetDirty(renderer);
			}
			*/

			// Expose the manual sorting order
			int newSortingLayerOrder = EditorGUILayout.IntField("Order in Layer", renderer.sortingOrder);
			if (newSortingLayerOrder != renderer.sortingOrder) {
				Undo.RecordObject(renderer, "Edit Sorting Order");
				renderer.sortingOrder = newSortingLayerOrder;
				EditorUtility.SetDirty(renderer);
			}
		}

		// Get the sorting layer names
		public string[] GetSortingLayerNames()
		{
			Type internalEditorUtilityType = typeof(InternalEditorUtility);
			PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
			return (string[])sortingLayersProperty.GetValue(null, new object[0]);
		}

		/*
		// Get the unique sorting layer IDs -- tossed this in for good measure
		public int[] GetSortingLayerUniqueIDs()
		{
			Type internalEditorUtilityType = typeof(InternalEditorUtility);
			PropertyInfo sortingLayerUniqueIDsProperty = internalEditorUtilityType.GetProperty("sortingLayerUniqueIDs", BindingFlags.Static | BindingFlags.NonPublic);
			return (int[])sortingLayerUniqueIDsProperty.GetValue(null, new object[0]);
		}
		*/
	}
}