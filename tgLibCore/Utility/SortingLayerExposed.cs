﻿using UnityEngine;

namespace TG {
	// Script for sorting layer and order in layer modification.
	[AddComponentMenu("tgLib/Utils/Sorting Layer Exposed")]
	public class SortingLayerExposed : MonoBehaviour
	{ }
}