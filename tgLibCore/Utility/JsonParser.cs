﻿using System;
using UnityEngine;

namespace TG {
	public static class JsonParser
	{
		public static T Deserialize<T>( string json )
		{
			return JsonUtility.FromJson<T>(json);
		}

		public static string Serialize<T>( T obj )
		{
			return JsonUtility.ToJson(obj);
		}
	}
}