﻿using UnityEngine;

namespace TG {
	public abstract class UIAnimator : MonoBehaviour
	{
		public virtual void Show( AnimatedUI ui, System.Action completeCb, params object[] data )
		{
			if (completeCb != null) {
				completeCb();
			}
		}

		public virtual void Hide( AnimatedUI ui, System.Action completeCb, params object[] data )
		{
			if (completeCb != null) {
				completeCb();
			}
		}
	}
}
