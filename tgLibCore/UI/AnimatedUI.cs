﻿using UnityEngine;

namespace TG {
	public class AnimatedUI : BaseUI
	{
	// Main panel of the popup, scaled when animated.
		public Transform mainPanel;

		public UIAnimator uiAnimator { get; private set; }

		protected override void Awake()
		{
			base.Awake();
			uiAnimator = GetComponent<UIAnimator>();
		}

		protected override void Start()
		{
			base.Start();
		}

		public override void Show( System.Action cb, params object[] args )
		{
			if (uiAnimator != null) {
				uiAnimator.Show(this, cb, args);
			} else {
				if (cb != null) {
					cb();
				}
			}
		}

		public override void Hide( System.Action cb, params object[] args )
		{
			if (uiAnimator != null) {
				uiAnimator.Hide(this, cb, args);
			} else {
				if (cb != null) {
					cb();
				}
			}
		}
	}
}