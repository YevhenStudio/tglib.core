﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

#if TG_TMPRO
using TMPro;
#endif

namespace TG {
	[ExecuteInEditMode]
	public class TextProxy : MonoBehaviour
	{
		// Unity UI text component.
		public Text uiText;
		// Text mesh component.
		public TextMesh textMesh;
		#if TG_TMPRO
		//TextMeshPro component
		public TextMeshPro textMeshPro;
		//TextMeshProUGUI component
		public TextMeshProUGUI meshProUGUI;
		#endif
		// Unity all child texts.
		public List<TextProxy> nestedTexts = new List<TextProxy>();

		// String value.
		string _value = "";

		void Awake()
		{
			if (uiText == null) {
				uiText = GetComponent<Text>();
				if (uiText == null) {
					uiText = GetComponentInChildren<Text>();
				}
			}
			if (uiText != null) { _value = uiText.text; }

#if TG_TMPRO
			if (textMeshPro == null) {
				textMeshPro = GetComponent<TextMeshPro>();
				if (textMeshPro == null) {
					textMeshPro = GetComponentInChildren<TextMeshPro>();
				}
			}
			if (textMeshPro != null) { _value = textMeshPro.text; }

			if (meshProUGUI == null) {
				meshProUGUI = GetComponent<TextMeshProUGUI>();
				if (meshProUGUI == null) {
					meshProUGUI = GetComponentInChildren<TextMeshProUGUI>();
				}
			}
			if (meshProUGUI != null) { _value = meshProUGUI.text; }
#endif

			if (textMesh == null) {
				textMesh = GetComponent<TextMesh>();
				if (textMesh == null) {
					textMesh = GetComponentInChildren<TextMesh>();
				}
			}
			if (textMesh != null) { _value = textMesh.text; }

			if (nestedTexts == null || nestedTexts.Count == 0) {
				var nested = GetComponentsInChildren<TextProxy>();
				foreach (var n in nested) {
					if (n != this) {
						nestedTexts.Add(n);
					}
				}
			}
		}

		public string text
		{
			get { return _value; }
			set {
				_value = value;
				if (uiText != null) uiText.text = _value;
				if (textMesh != null) textMesh.text = _value;
#if TG_TMPRO				
				if (textMeshPro != null) textMeshPro.text = _value;
				if (meshProUGUI != null) meshProUGUI.text = _value;
#endif
				if (nestedTexts != null) nestedTexts.ForEach(n => n.text = _value);
			}
		}

		public Color color
		{
			get {
				if (uiText != null) return uiText.color;
				if (textMesh != null) return textMesh.color;
#if TG_TMPRO				
				if (textMeshPro != null) return textMeshPro.color;
				if (meshProUGUI != null) return meshProUGUI.color;
#endif
				return Color.white;
			}
			set {
				if (uiText != null) uiText.color = value;
				if (textMesh != null) textMesh.color = value;
#if TG_TMPRO
				if (textMeshPro != null) textMeshPro.color = value;
				if (meshProUGUI != null) meshProUGUI.color = value;
#endif
				if (nestedTexts != null) nestedTexts.ForEach(n => n.color = value);
			}
		}
	}
}