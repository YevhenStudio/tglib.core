﻿using UnityEngine;
#if TG_DOTWEEN
using DG.Tweening;
#endif

namespace TG {
	public class UIAnimatorSlide : UIAnimator
	{
		// Show animation time.
		public float showTime = 0.5f;
		// Hide animation time.
		public float hideTime = 0.1f;
		
		#if TG_DOTWEEN
		// Ease type used for animation.
		public Ease easeType = Ease.InCirc;
		#endif

		public override void Show( AnimatedUI ui, System.Action completeCb, params object[] data )
		{
			if (ui == null || ui.mainPanel == null) return;

			var mainPanel = ui.mainPanel as RectTransform;
			var hiddenPos = new Vector3(0, -mainPanel.rect.height, 0);
			mainPanel.anchoredPosition3D = hiddenPos;
			AnimateTo(mainPanel, Vector3.zero, showTime, completeCb);
		}

		public override void Hide( AnimatedUI ui, System.Action completeCb, params object[] data )
		{
			if (ui == null || ui.mainPanel == null) return;

			var mainPanel = ui.mainPanel as RectTransform;
			var hiddenPos = new Vector3(0, -mainPanel.rect.height, 0);
			AnimateTo(mainPanel, hiddenPos, hideTime, completeCb);
		}

		void AnimateTo( RectTransform mainPanel, Vector3 targetPos, float animTime, System.Action completeCb )
		{
			#if TG_DOTWEEN
			DOTween.To(() => mainPanel.anchoredPosition3D, x=> mainPanel.anchoredPosition3D = x, targetPos, animTime)
				.SetEase(easeType)
				.SetUpdate(true)
				.OnComplete(delegate { if (completeCb != null) completeCb(); });
			#else
				if (completeCb != null) completeCb();
			#endif
		}
	}
}
