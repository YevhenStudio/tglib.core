﻿using UnityEngine;

namespace TG {
	/// Base class for ui dialogs.
	public class BaseUI : MonoBehaviour
	{
		// Canvas used for rendering.
		public Canvas canvas;

		// Is animator used for animations.
		protected bool _useAnimator;
		// Animator used to show/hide ui.
		protected Animator _animator;

		[ContextMenu ("AssignCamera")]
		public void AssignCamera()	{
			if (Application.isPlaying) {
				if (canvas != null && Core.Get<CameraManager>() != null) {
					canvas.worldCamera = Core.Get<CameraManager>().main;
				}
			} else {
				canvas.renderMode = RenderMode.ScreenSpaceCamera;
				canvas.worldCamera = Camera.main;
			}
		}

		protected virtual void Awake()
		{
			if (canvas == null) {
				canvas = GetComponentInChildren<Canvas>();
			}

			_animator = GetComponent<Animator>();
			if (!_useAnimator && _animator != null) _animator.enabled = false;
		}

		protected virtual void Start()
		{
			AssignCamera();
		}

		public void Show() { Show(null, new object[0]); }
		public virtual void Show( System.Action cb, params object[] args )
		{
			if (cb != null) cb();
		}

		public void Hide() { Hide(null, new object[0]); }
		public virtual void Hide( System.Action cb, params object[] args )
		{
			if (cb != null) cb();
		}
		public void HideAndDestroy( System.Action cb, params object[] args )
		{
			Hide(() => { if (cb != null) cb(); Destroy(gameObject); }, args);
		}

		public void Activate() { Activate(null, new object[0]); }
		public void Activate( System.Action cb, params object[] args )
		{
			gameObject.SetActive(true);
			Show(cb, args);
		}

		public void Deactivate() { Deactivate(null, new object[0]); }
		public void Deactivate( System.Action cb, params object[] args )
		{
			Hide(() => {
				if (cb != null) cb();
				gameObject.SetActive(false);
			}, args);
		}
	}
}