﻿using UnityEngine;
using System.Collections;

namespace TG {
	public class LoadingIndicator : MonoBehaviour
	{
		public bool isVisible = false;
		public TextMesh progressText;

		public static LoadingIndicator instance;

		Renderer _renderer;
		void Awake()
		{
			instance = this;

			DontDestroyOnLoad(gameObject);

			_renderer = GetComponent<Renderer>();
			if (_renderer != null) _renderer.enabled = isVisible;
		}

		public void Show( float progress = -1.0f )
		{
			isVisible = true;
			if (_renderer != null) _renderer.enabled = true;

			SetProgress(progress);
		}

		public void Hide()
		{
			isVisible = false;
			if (_renderer != null) _renderer.enabled = false;
			if (progressText != null) progressText.gameObject.SetActive(false);
		}

		public void SetProgress( float progress )
		{
			if (progressText == null) { return; }

			progressText.gameObject.SetActive(progress >= 0f ? true : false);
			progressText.text = (int)(progress*100)+"%";
		}
	}
}
