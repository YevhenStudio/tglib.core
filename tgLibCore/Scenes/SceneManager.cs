﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TG {
	// Interface for scenes without native Unity scene management.
	public abstract class IPseudoScene : MonoBehaviour
	{
		// Called on current scene before changing to anouther scene.
		virtual public bool ShouldExit() { return true; }
		// Called on the next scene to check if we can enter this scene.
		virtual public bool ShouldEnter() { return true; }
		// Called before OnEnter of the next scene.
		virtual public void OnExit() {}
		// Called after changing current scene.
		// Also called on Awake for starting scene.
		virtual public void OnEnter( object arg ) {}
		// Method used in crossfade scene switching.
		virtual public void OnBeforeEnter( object arg ) {}
		// Method used in crossfade scene switching.
		virtual public void OnBeforeExit() {}

		// Name of the scene.
		public abstract string sceneName { get; }
		// Is scene in transition used by crossfade switching.
		public bool inTransition { get; set; }
	}

	// Methods for scene switching supports native unity scenes and pseudo scenes.
	public class SceneManager : MonoBehaviour
	{
		public bool inTransition { get; private set; }

		// Scene is changing to.
		[HideInInspector]
		public string nextLevelName;

		private LoadingIndicator _loadingIndicator;

		void Awake ()
		{
			if (Core.Exists<SceneManager>()) {
				Destroy(gameObject);
			} else {
				Core.Register(this);
				DontDestroyOnLoad(gameObject);
			}

			_loadingIndicator = Core.Get<LoadingIndicator>();
		}

		void OnDestroy()
		{
			if (Core.Get<SceneManager>() == this) {
				Core.Unregister(this);
			}
		}

		void Start()
		{
			if (
				startSceneLevel == "" || Application.loadedLevelName == startSceneLevel
			) {
				OnAwake();
			}
		}

		// Restarts currently loaded scene.
		public void Restart( float delay = 0f )
		{
			StartCoroutine(RestartCor(delay));
		}

		// Loads new level.
		public void LoadLevel( string name )
		{
			if (!inTransition) {
				StartCoroutine(LoadLevelCor(name));
			}
		}

		IEnumerator RestartCor( float delay )
		{
			if (inTransition) { yield break; }

			inTransition = true;

			yield return new WaitForSeconds(delay);
			Application.LoadLevel(Application.loadedLevel);

			inTransition = false;
		}

		IEnumerator LoadLevelCor( string levelName )
		{
			Debug.Log("Loading level " + levelName);

			inTransition = true;
			if (_loadingIndicator != null) _loadingIndicator.Show(0f);

			nextLevelName = levelName;

			if (Application.CanStreamedLevelBeLoaded(levelName)) {
				AsyncOperation async = Application.LoadLevelAsync(levelName);
				while (!async.isDone) {
					yield return async.isDone;

					if (_loadingIndicator != null) {
						_loadingIndicator.SetProgress(async.progress);
					}
				}
					
//				 if this a start scene level initialize it.
				//if (startSceneLevel == levelName) {
				OnAwake();
//			}
			} else {
				Debug.LogErrorFormat("Level cannot be loaded: {0}. Add scene to build list.", levelName);
			}

			if (_loadingIndicator != null) _loadingIndicator.Hide();
			inTransition = false;
		}


		//
		// Pseudo scenes management
		//

		// List of scene available.
		public List<IPseudoScene> scenes;
		// Start scene.
		public IPseudoScene startScene;
		// Debug start scene.
		public IPseudoScene debugStartScene;
		// Start scene level name. If empty string that pseudo scene is always initialized.
		public string startSceneLevel;
		// Currently played scene.
		[HideInInspector]
		public IPseudoScene currentScene;
		// Scene is changing to.
		[HideInInspector]
		public string nextSceneName;

		void OnAwake()
		{
			if (scenes != null) {
				foreach (var scene in scenes) {
					if (scene == null) {
						Debug.LogError("Null reference in scene list.");
					}
	#if UNITY_EDITOR
					if (scene == debugStartScene || (debugStartScene == null && scene == startScene)) {
	#else
					if (scene == startScene) {
	#endif
						scene.gameObject.SetActive(true);
						scene.OnBeforeEnter(null);

						if (currentScene != null && currentScene != scene) {
							currentScene.OnExit();
						}

						scene.OnEnter(null);
						currentScene = scene;
					} else {
						scene.gameObject.SetActive(false);
					}
				}
			}
		}

		public void LoadScene( string name, object arg = null )
		{
			StartCoroutine(TryLoadSceneCor(name, arg, false));
		}

		public void LoadSceneCrossFade( string sceneName, object arg = null )
		{
			StartCoroutine(TryLoadSceneCor(sceneName, arg, true));
		}

		IEnumerator TryLoadSceneCor( string sceneName, object arg, bool crossFade )
		{
			if (inTransition) {
				if (nextSceneName != sceneName) {
					Debug.LogError("Cannot change scene. Already in transition.");
				}
				yield break;
			}

			var nextScene = SceneByName(sceneName);
			if (nextScene == null) {
				Debug.LogError("Scene not found: " + sceneName);
				yield break;
			}

			inTransition = true;
			nextSceneName = nextScene.sceneName;
			nextScene.gameObject.SetActive(true);

			// wait for next scene initialization
			yield return 1;
			if (currentScene != null && !currentScene.ShouldExit()) {
				inTransition = false;
				yield break;
			}

			if (!nextScene.ShouldEnter()) {
				inTransition = false;
				nextScene.gameObject.SetActive(false);
				yield break;
			}

			if (crossFade) {
				if (currentScene != null) {
					currentScene.inTransition = false;
					currentScene.OnBeforeExit();
				}
				nextScene.inTransition = false;
				nextScene.OnBeforeEnter(arg);
				// wait for scene enter/exit transitions to finish.
				while (nextScene.inTransition || currentScene.inTransition) {
					yield return 1;
				}
			}

			if (currentScene != null) {
				currentScene.OnExit();
				currentScene.gameObject.SetActive(false);
			}

			nextScene.OnEnter(arg);

			currentScene = nextScene;
			inTransition = false;
		}

		public void SetStartScene( string sceneName )
		{
			startScene = SceneByName(sceneName);
		}

		IPseudoScene SceneByName( string sceneName )
		{
			return scenes.Find(s => s.sceneName == sceneName);
		}
	}
}
