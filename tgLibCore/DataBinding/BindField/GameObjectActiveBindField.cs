﻿using UnityEngine;

namespace TG {
	public class GameObjectActiveBindField : BindFieldBase<bool, GameObject>
	{
		protected override void Setup()
		{
			view = gameObject;
			Setup(() => view.activeSelf, x => view.SetActive(x), null);
		}
	}
}
