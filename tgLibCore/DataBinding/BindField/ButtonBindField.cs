﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace TG {
	// Class for binding action value to Button.
	[RequireComponent(typeof(Button))]
	public class ButtonBindField : BindFieldBase<UnityAction, Button>
	{
		protected override void Setup() {
			Setup(() => {
				return null;
			}, x => {
				view.onClick.RemoveAllListeners();
				view.onClick.AddListener(x);
			}, null);
		}
	}
}
