﻿using UnityEngine;

namespace TG {
	// Class for binding string value to TextProxy.
	[RequireComponent(typeof(TextProxy))]
	public class TextProxyBindField : BindFieldBase<string, TextProxy>
	{
		// Format applied, when converting input object to string.
		public string stringFormat;

		protected override void Setup() {
			var processor = new DefaultStringFieldProcessor(stringFormat);
			Setup(() => view.text, x => view.text = x, processor);
		}
	}
}
