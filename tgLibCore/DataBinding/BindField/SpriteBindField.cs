﻿using UnityEngine;

namespace TG {
	// Class for binding sprite value to SpriteRenderer.
	[RequireComponent(typeof(SpriteRenderer))]
	public class SpriteBindField : BindFieldBase<Sprite, SpriteRenderer>
	{
		protected override void Setup() {
			Setup(() => view.sprite, x => view.sprite = x, null);
		}
	}
}
