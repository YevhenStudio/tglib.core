﻿using UnityEngine;
using UnityEngine.UI;

namespace TG {
	// Class for binding sprite value to SpriteRenderer.
	[RequireComponent(typeof(Image))]
	public class ImageBindField : BindFieldBase<Sprite, Image>
	{
		protected override void Setup() {
			Setup(() => view.sprite, x => view.sprite = x, null);
		}
	}
}
