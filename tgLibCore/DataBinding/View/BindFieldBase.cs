﻿using UnityEngine;

namespace TG {
	// Class for binding string value to TextProxy.
	public abstract class BindFieldBase<T, V> : IBindField where V : class
	{
		// View, where data is bound.
		public V view;
		// True if this fields accepts null values.
		public bool allowNull = true;
		// If true default value is got from component.
		public bool autoDefaultValue = true;
		// Default value for field.
		// Used when input data is null. And allowNull is true.
		public T defaultValue;

		/// <summary>
		/// Used in place of <c>System.Func</c>, which is not available in mscorlib.
		/// </summary>
		public delegate T Getter<out T>();
		/// <summary>
		/// Used in place of <c>System.Action</c>.
		/// </summary>
		public delegate void Setter<in T>(T pNewValue);

		Getter<T> _getter;
		Setter<T> _setter;
		FieldProcessor<T> _processor;

		// Binds object to view.
		// Returns true if binding was successfull.
		public override bool Bind( System.Object data )
		{
			if (data == null) {
				if (allowNull) {
					return SetData(defaultValue);
				}
#if UNITY_EDITOR
				Debug.LogError("Null not allowed for: " + key);
#endif
				return false;
			}

			return SetData(data);
		}

		protected override void Awake()
		{
			base.Awake();
			GetView();
			Setup();
			GetDefaultValue();
		}

		void GetView()
		{
			if (view == null) {
				if (typeof(V).IsSubclassOf(typeof(MonoBehaviour))) {
					view = GetComponent<V>();
				}

#if UNITY_EDITOR
				if (view == null) {
					Debug.LogError("View not found: " + typeof(V));
				}
#endif
			}
		}

		void GetDefaultValue()
		{
			if (autoDefaultValue) {
				if (view != null && _getter != null) {
					defaultValue = _getter();
				}
			}
		}

		protected abstract void Setup();

		protected void Setup(Getter<T> getter, Setter<T> setter, FieldProcessor<T> processor = null)
		{
			_getter = getter;
			_setter = setter;
			_processor = processor;
		}

		protected virtual bool SetData(System.Object data)
		{
			if (view == null) {
				return false;
			}

			if (_setter == null) {
#if UNITY_EDITOR
				Debug.LogError("Field setter is null: " + key);
#endif
				return false;
			}

			if (_processor != null) {
				_setter(_processor.Process(data));
			} else {
				_setter((T)data);
			}
			return true;
		}
	}
}
