﻿using UnityEngine;

namespace TG {
	// Base class for view fields with binding.
	public class IBindField : MonoBehaviour
	{
		// Key to separate field from other.
		public string key;
		// True if this field requires constant updates.
		public bool isReactive;
		// True when field requires update.
		public bool isDirty { get; set; }

		public event System.Func<IBindField, System.Object, System.Object> onPreProcess;

		// Binds object to view.
		// Returns true if binding was successfull.
		public virtual bool Bind( System.Object data )
		{
			return false;
		}

		protected virtual void Awake()
		{
			CheckKey();
		}

		protected virtual void Reset()
		{
			CheckKey();
		}

		// Sets field key from name, if it's not set
		void CheckKey()
		{
			if (string.IsNullOrEmpty(key)) {
				key = name;
			}
		}
	}
}
