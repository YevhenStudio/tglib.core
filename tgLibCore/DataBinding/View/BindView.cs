﻿using UnityEngine;
using System.Collections.Generic;

namespace TG {
	// Class for binding values to views,
	public class BindView : MonoBehaviour
	{
		public List<IBindField> fields = new List<IBindField>();

		// Uses null if noting is passed in params.
		public bool useDefaultNull;
		// True when all fields must be set.
		public bool requiresAll;

		// Not used.
		public bool isDirty { get; set; }

		public event System.Action<BindView, BindParams> OnBeforeBind;
		public event System.Action<BindView, BindParams> OnBind;

		public IBindField GetField( string key )
		{
			return fields.Find(f => f.key == key);
		}

		public T GetField<T>( string key ) where T : IBindField
		{
			return (T)GetField(key);
		}

		public bool Bind(BindParams parms)
		{
			if (OnBeforeBind != null) {
				OnBeforeBind(this, parms);
			}

			var rv = true;

			int numSet = 0;
			foreach (var f in fields) {
				if (f == null) {
					continue;
				}

				if (parms.values.ContainsKey(f.key)) {
					f.Bind(parms.values[f.key]);
					numSet++;
				} else {
					if (useDefaultNull) {
						var r = f.Bind(null);
						if (r) {
							numSet++;
						} else {
							if (requiresAll) {
#if UNITY_EDITOR
								Debug.LogError("Field not set: " + f.key);
#endif
							}
						}
					} else {
						rv = false;
					}
				}
			}

			if (requiresAll && numSet != fields.Count) {
#if UNITY_EDITOR
				Debug.LogError("Not all fields set.");
#endif
			}

			if (OnBind != null) {
				OnBind(this, parms);
			}

			return rv;
		}

		public bool Bind(string key, object val)
		{
			return DoBind(key, val);
		}

		bool DoBind(string key, object val)
		{
			foreach (var f in fields) {
				if (f.key == key) {
					return f.Bind(val);
				}
			}

			return false;
		}

		[ContextMenu ("Fill Bind Fields")]
		void FillFields()
		{
			var f = GetComponentsInChildren<IBindField>(true);
			fields.Clear();
			fields.AddRange(f);
		}

		[ContextMenu ("Print Bind Fields")]
		void PrintFields()
		{
			foreach (var f in fields) {
				Debug.Log("Field: " + f.key, f);
			}
		}
	}
}
