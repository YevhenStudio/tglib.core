﻿namespace TG {
	public class BindFieldAttribute : System.Attribute
	{
		public string name;

		public BindFieldAttribute(string n)
		{
			name = n;
		}
	}
}
