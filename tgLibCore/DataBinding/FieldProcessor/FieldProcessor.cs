﻿public abstract class FieldProcessor<T>
{
	public abstract T Process( System.Object data );
}
