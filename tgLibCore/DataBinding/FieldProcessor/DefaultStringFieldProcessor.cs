﻿using UnityEngine;

namespace TG {
	public class DefaultStringFieldProcessor : FieldProcessor<string>
	{
		string _stringFormat;

		public DefaultStringFieldProcessor( string stringFormat )
		{
			_stringFormat = stringFormat;
		}

		public override string Process( System.Object data )
		{
			string val;
			if (!string.IsNullOrEmpty(_stringFormat)) {
				if (data is string) {
					val = data as string;
				} else if (data is int) {
					val = ((int)data).ToString(_stringFormat);
				} else if (data is float) {
					val = ((float)data).ToString(_stringFormat);
				} else if (data is double) {
					val = ((double)data).ToString(_stringFormat);
				} else if (data is long) {
					val = ((long)data).ToString(_stringFormat);
				} else if (data is System.DateTime) {
					val = ((System.DateTime)data).ToString(_stringFormat);
				} else {
#if UNITY_EDITOR
					Debug.LogError("Formatter not supported for: " + data.GetType());
#endif
					val = data.ToString();
				}
			} else {
				val = data.ToString();
			}

			return val;
		}
	}
}
