﻿using System.Collections.Generic;
using System.Reflection;

namespace TG {
	// Parameters passed to BindView component.
	public class BindParams
	{
		public Dictionary<string, object> values = new Dictionary<string, object>();

		// Adds single key/value to params list.
		public BindParams Add( string k, object v )
		{
			values.Add(k, v);
			return this;
		}

		// Adds list of bind fields from data object class.
		public BindParams Add( System.Object data )
		{
			var dataType = data.GetType();
			var attrs = System.Attribute.GetCustomAttributes(dataType);
			foreach (System.Attribute attr in attrs) {
				if (attr is BindClassAttribute) {
					var bindAttr = (BindClassAttribute)attr;
					if (bindAttr.bindPublic) {
						// iterate through members and add values to params
						var members = dataType.GetMembers();
						foreach (var member in members) {
							var memberAttrs = member.GetCustomAttributes(typeof(BindFieldAttribute), true);
							string key = null;
							foreach (var mAttr in memberAttrs) {
								if (mAttr is BindFieldAttribute) {
									var bindFieldAttr = (BindFieldAttribute)mAttr;
									key = bindFieldAttr.name;
								}
							}

							if (key == null) {
								key = member.Name;
							}

							object val = null;
							bool doAdd = false;
							if (member is FieldInfo) {
								val = ((FieldInfo)member).GetValue(data);
								doAdd = true;
							} else if (member is PropertyInfo) {
								val = ((PropertyInfo) member).GetValue(data, null);
								doAdd = true;
							}

							if (doAdd) {
								values.Add(key, val);
							}
						}
					}
					break;
				}
			}

			return this;
		}
	}
}
